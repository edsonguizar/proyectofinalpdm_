import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:date_format/date_format.dart';
import 'DBHelper.dart';
import 'guardarropa/agregarPrenda.dart';
import 'dart:io' as io;
import 'package:carousel_pro/carousel_pro.dart';

class Historial extends StatefulWidget {
  Historial({Key key}) : super(key: key);

  @override
  _HistorialState createState() => _HistorialState();
}

class _HistorialState extends State<Historial> {
  String f = 'Selecciona una fecha';
  var db = DBHelper();

  bool fechaSeleccionada = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Historial',
          style: TextStyle(fontSize: 30.0),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Calendar(
              isExpandable: true,
              onDateSelected: (fecha) {
                print(formatDate(fecha, [dd, mm, yyyy]));
                setState(() {
                  f = formatDate(fecha, [dd, mm, yyyy]);
                  fechaSeleccionada = true;
                });
              },
            ),
            !fechaSeleccionada
                ? Center(child: Text(f))
                : FutureBuilder(
                    future: getOutfit(f),
                    builder: (context, snapshot) {
                      if (snapshot.hasError) print(snapshot.error);

                      var size = MediaQuery.of(context).size;
                      Map outfit = snapshot.data;

                      return !snapshot.hasData
                          ? Center(
                              child: Text('No hay un outfit en el día elegido'))
                          : outfit.isEmpty
                              ? Center(
                                  child: Text(
                                      'No hay un outfit en el día elegido'))
                              : FutureBuilder(
                                  future:
                                      db.getPrendasOutfit(outfit['id_outfit']),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasError)
                                      print(snapshot.error);

                                    List<Prenda> prendasOutfit = snapshot.data;

                                    return !snapshot.hasData
                                        ? Center(
                                            child: Text('No hay imágenes'),
                                          )
                                        : prendasOutfit.isEmpty
                                            ? Center(
                                                child: Text('No hay imágenes'),
                                              )
                                            : Column(children: [
                                                ListTile(
                                                  onTap: () {},
                                                  title: Column(
                                                    children: <Widget>[
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: <Widget>[
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Text(
                                                                outfit[
                                                                    'nombre'],
                                                                style:
                                                                    TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                ),
                                                              ),
                                                              Text(
                                                                outfit[
                                                                    'descripcion'],
                                                                style:
                                                                    TextStyle(
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .italic,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          RaisedButton(
                                                            onPressed: () {
                                                              db.eliminarHistorial(
                                                                  outfit[
                                                                      'id_outfit'],
                                                                  f);
                                                              setState(() {});
                                                            },
                                                            child: Text(
                                                                'Eliminar'),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(
                                                        height: 10.0,
                                                      )
                                                    ],
                                                  ),
                                                  subtitle: SizedBox(
                                                    width: size.width,
                                                    height: size.height / 2.5,
                                                    child: Carousel(
                                                      images: prendasOutfit
                                                          .map((p) {
                                                        return FileImage(
                                                            io.File(p.ruta),
                                                            scale: 0.1);
                                                      }).toList(),
                                                      dotSize: 5.0,
                                                      indicatorBgPadding: 10.0,
                                                      animationCurve:
                                                          Curves.bounceOut,
                                                      animationDuration:
                                                          Duration(seconds: 1),
                                                      autoplayDuration:
                                                          Duration(seconds: 3),
                                                      boxFit: BoxFit.contain,
                                                    ),
                                                  ),
                                                ),
                                              ]);
                                  },
                                );
                    },
                  ),
          ],
        ),
      ),
    );
  }

  Future<Map> getOutfit(String f) async {
    var db = await DBHelper();
    Map outfit = await db.getOutfit(f);
    return outfit;
  }
}
