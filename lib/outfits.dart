import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'DBHelper.dart';
import 'guardarropa/agregarPrenda.dart';
import 'guardarropa.dart';
import 'dart:io' as io;
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:date_format/date_format.dart';

class Outfit {
  int id;
  String _nombre;
  String _descripcion;

  Outfit(this._nombre, this._descripcion);

  Outfit.map(dynamic obj) {
    this._nombre = obj['nombre'];
    this._descripcion = obj['descripcion'];
  }

  String get nombre => _nombre;
  String get descripcion => _descripcion;

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['nombre'] = _nombre;
    map['descripcion'] = _descripcion;

    return map;
  }

  void setID(int id) {
    this.id = id;
  }
}

class Outfits extends StatefulWidget {
  Outfits({Key key}) : super(key: key);

  @override
  _OutfitsState createState() => _OutfitsState();
}

class _OutfitsState extends State<Outfits> {
  var db = DBHelper();

  @override
  Widget build(BuildContext context) {
    bool pressed = false;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Outfits',
          style: TextStyle(fontSize: 30.0),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.add,
                size: 30.0,
              ),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return AgregarOutfit();
                  },
                ));
              }),
        ],
      ),
      body: FutureBuilder(
        future: db.getOutfits(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          List<Outfit> outfits = snapshot.data;

          var size = MediaQuery.of(context).size;

          List<FutureBuilder> carouseles = List<FutureBuilder>();

          for (int i = 0; i < outfits.length; i++) {
            FutureBuilder f = FutureBuilder(
              future: db.getPrendasOutfit(outfits[i].id),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);

                List<Prenda> prendasOutfit = snapshot.data;

                return !snapshot.hasData
                    ? Center(
                        child: Text('No hay imágenes'),
                      )
                    : prendasOutfit.isEmpty
                        ? Center(
                            child: Text('No hay imágenes'),
                          )
                        : SizedBox(
                            width: size.width,
                            height: size.height / 2.5,
                            child: Carousel(
                              images: prendasOutfit.map((p) {
                                return FileImage(io.File(p.ruta), scale: 0.1);
                              }).toList(),
                              dotSize: 5.0,
                              indicatorBgPadding: 10.0,
                              animationCurve: Curves.bounceOut,
                              animationDuration: Duration(seconds: 1),
                              autoplayDuration: Duration(seconds: 3),
                              boxFit: BoxFit.contain,
                            ),
                          );
              },
            );

            carouseles.add(f);
          }

          return !snapshot.hasData
              ? Center(
                  child: Text('No hay outfits'),
                )
              : outfits.isEmpty
                  ? Center(
                      child: Text('No hay outfits'),
                    )
                  : ListView.builder(
                      itemCount: outfits.length,
                      itemBuilder: (context, i) {
                        return Column(
                          children: <Widget>[
                            ListTile(
                              onTap: () {},
                              title: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            outfits[i].nombre,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            outfits[i].descripcion,
                                            style: TextStyle(
                                              fontStyle: FontStyle.italic,
                                            ),
                                          ),
                                        ],
                                      ),
                                      RaisedButton(
                                        //highlightColor: Colors.grey,
                                        onPressed: () {
                                          _usarOutfit(
                                              formatDate(
                                                  DateTime.now(), [d, m, yyyy]),
                                              outfits[i].id);
                                          showDialog(
                                              context: context,
                                              builder: (context) {
                                                return AlertDialog(
                                                  title: Text(
                                                      'Outfit marcado como utilizado'),
                                                  content: Text(
                                                      'El outfit ha sido marcado como utilizado. Para mostrar el historial de outfits, selecciona la opción "Historial" en el menú principal.'),
                                                );
                                              });
                                        },
                                        splashColor: Colors.white30,
                                        child: Text(
                                          'USAR HOY',
                                          style: TextStyle(
                                            //fontSize: 20.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                        color: Color(0xff2c0a2c),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  )
                                ],
                              ),
                              subtitle: carouseles[i],
                            ),
                            Divider(
                              height: 20.0,
                              color: Colors.grey,
                            ),
                          ],
                        );
                      },
                    );
        },
      ),
    );
  }

  void _usarOutfit(String fecha, int id) async {
    var db = await DBHelper();
    await db.guardarFechaOutfit(fecha, id);
  }
}

class AgregarOutfit extends StatefulWidget {
  final List<Prenda> prendas = new List();

  @override
  _AgregarOutfitState createState() => _AgregarOutfitState();
}

class _AgregarOutfitState extends State<AgregarOutfit> {
  var db = DBHelper();

  List<Categoria> cats;

  var nom = TextEditingController();
  var desc = TextEditingController();

  void _guardarOutfit() async {
    var db = await DBHelper();
    var o = Outfit(nom.text, desc.text);
    int idO = await db.guardarOutfit(o);
    for (int i = 0; i < widget.prendas.length; i++) {
      await db.guardarPrendaOutfit(idO, widget.prendas[i].id);
    }

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Agregar outfit'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _guardarOutfit,
            tooltip: 'Guardar outfit',
          ),
          IconButton(
            icon: Icon(Icons.add),
            tooltip: 'Agregar prenda',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) {
                  return Scaffold(
                    appBar: AppBar(
                      title: Text(
                        'Guardarropa',
                        style: TextStyle(fontSize: 30.0),
                      ),
                    ),
                    body: FutureBuilder(
                      future: db.getCategorias(),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) print(snapshot.error);

                        cats = snapshot.data;

                        return !snapshot.hasData
                            ? Center(
                                child: Text('No hay categorías'),
                              )
                            : cats.isEmpty
                                ? Center(
                                    child: Text('No hay categorías'),
                                  )
                                : ListView.builder(
                                    itemCount: cats.length,
                                    itemBuilder: (BuildContext context, int i) {
                                      return Container(
                                        child: Column(
                                          children: <Widget>[
                                            ListTile(
                                              title: Text(
                                                  cats[i].nombre.toString()),
                                              onTap: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(builder:
                                                        (BuildContext context) {
                                                  return Scaffold(
                                                    appBar: AppBar(
                                                      title:
                                                          Text(cats[i].nombre),
                                                    ),
                                                    body: FutureBuilder(
                                                      future: db.getPrendas(
                                                          cats[i].id),
                                                      builder:
                                                          (context, snapshot) {
                                                        if (snapshot.hasError)
                                                          print(snapshot.error);

                                                        List<Prenda> prendas =
                                                            snapshot.data;

                                                        var size =
                                                            MediaQuery.of(
                                                                    context)
                                                                .size;

                                                        List<StaggeredTile>
                                                            tiles = new List();

                                                        for (var i = 0;
                                                            i < prendas.length;
                                                            i++) {
                                                          tiles.add(
                                                              StaggeredTile.fit(
                                                                  2));
                                                        }

                                                        return !snapshot.hasData
                                                            ? Center(
                                                                child: Text(
                                                                    'No hay prendas en la categoría'),
                                                              )
                                                            : prendas.isEmpty
                                                                ? Center(
                                                                    child: Text(
                                                                        'No hay prendas en la categoría'),
                                                                  )
                                                                : StaggeredGridView
                                                                    .count(
                                                                    crossAxisCount:
                                                                        4,
                                                                    children:
                                                                        prendas.map(
                                                                            (p) {
                                                                      return InkWell(
                                                                        onTap:
                                                                            () {
                                                                          setState(
                                                                              () {
                                                                            widget.prendas.add(p);
                                                                          });
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                        },
                                                                        child:
                                                                            Card(
                                                                          margin:
                                                                              EdgeInsets.all(5.0),
                                                                          child:
                                                                              Container(
                                                                            margin:
                                                                                EdgeInsets.all(2.0),
                                                                            //padding: EdgeInsets.all(1.0),
                                                                            child:
                                                                                Column(
                                                                              children: <Widget>[
                                                                                Image.file(io.File(p.ruta)),
                                                                                Padding(
                                                                                  padding: EdgeInsets.all(3.0),
                                                                                ),
                                                                                Divider(
                                                                                  color: Colors.grey,
                                                                                  height: 4.0,
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.all(3.0),
                                                                                ),
                                                                                Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                                  children: <Widget>[
                                                                                    Text(p.marca),
                                                                                    Container(
                                                                                      width: 20.0,
                                                                                      height: 10.0,
                                                                                      color: Color(int.parse(p.color)),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      );
                                                                    }).toList(),
                                                                    mainAxisSpacing:
                                                                        4.0,
                                                                    crossAxisSpacing:
                                                                        4.0,
                                                                    staggeredTiles:
                                                                        tiles,
                                                                  );
                                                      },
                                                    ),
                                                  );
                                                }));
                                              },
                                              trailing: Icon(
                                                  Icons.keyboard_arrow_right),
                                            ),
                                            Divider(
                                              color: Colors.grey,
                                              height: 0.0,
                                            )
                                          ],
                                        ),
                                      );
                                    },
                                  );
                      },
                    ),
                  );
                },
              ));
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: nom,
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.words,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Nombre',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: desc,
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: 'Descripción',
              ),
            ),
          ),
          Flexible(
            child: widget.prendas.isEmpty
                ? Center(child: Text('Selecciona una prenda'))
                : ListView.builder(
                    itemCount: widget.prendas.length,
                    itemBuilder: (BuildContext context, int i) {
                      return Column(
                        children: <Widget>[
                          ListTile(
                            onTap: () {},
                            //contentPadding: EdgeInsets.all(0.0),
                            title: Row(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                    color: Colors.grey,
                                  )),
                                  child: Image.file(
                                    io.File(widget.prendas[i].ruta),
                                    height: 200.0,
                                    width: 200.0,
                                  ),
                                ),
                                SizedBox(
                                  width: 20.0,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Marca:'),
                                    Text(widget.prendas[i].marca),
                                    Text('Color:'),
                                    Container(
                                      height: 20.0,
                                      width: 200.0,
                                      color: Color(
                                          int.parse(widget.prendas[i].color)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            color: Colors.grey,
                            height: 10.0,
                          )
                        ],
                      );
                    },
                  ),
          ),
        ],
      ),
    );
  }
}
