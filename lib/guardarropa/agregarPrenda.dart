import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../DBHelper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'dart:io' as io;
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'dart:math';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

double alto(BuildContext context) => MediaQuery.of(context).size.height;
double ancho(BuildContext context) => MediaQuery.of(context).size.width;

class Prenda {
  int id;
  String _color;
  String _marca;
  String _ruta;

  Prenda(this._color, this._marca, this._ruta);

  Prenda.map(dynamic obj) {
    this._color = obj['color'];
    this._marca = obj['marca'];
    this._ruta = obj['ruta'];
  }

  String get color => _color;
  String get marca => _marca;
  String get ruta => _ruta;

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['color'] = _color;
    map['marca'] = _marca;
    map['ruta'] = _ruta;

    return map;
  }

  void setID(int id) {
    this.id = id;
  }
}

class PrendasCat extends StatefulWidget {
  final int idCat;
  final String nombreCat;

  PrendasCat(this.idCat, this.nombreCat);

  @override
  _PrendasCatState createState() => _PrendasCatState();
}

class _PrendasCatState extends State<PrendasCat> {
  var db = new DBHelper();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.nombreCat),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new AgregarPrenda(widget.idCat)))
                    .then((value) {
                  setState(() {});
                }),
            tooltip: 'Agregar prenda',
          )
        ],
      ),
      body: FutureBuilder(
        future: db.getPrendas(widget.idCat),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          List<Prenda> prendas = snapshot.data;

          var size = MediaQuery.of(context).size;

          List<StaggeredTile> tiles = new List();

          for (var i = 0; i < prendas.length; i++) {
            tiles.add(StaggeredTile.fit(2));
          }

          return !snapshot.hasData
              ? Center(
                  child: Text('No hay prendas en la categoría'),
                )
              : prendas.isEmpty
                  ? Center(
                      child: Text('No hay prendas en la categoría'),
                    )
                  : StaggeredGridView.count(
                      crossAxisCount: 4,
                      children: prendas.map((p) {
                        return Card(
                          margin: EdgeInsets.all(5.0),
                          child: Container(
                            margin: EdgeInsets.all(2.0),
                            //padding: EdgeInsets.all(1.0),
                            child: Column(
                              children: <Widget>[
                                Image.file(io.File(p.ruta)),
                                Padding(
                                  padding: EdgeInsets.all(3.0),
                                ),
                                Divider(
                                  color: Colors.grey,
                                  height: 4.0,
                                ),
                                Padding(
                                  padding: EdgeInsets.all(3.0),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(p.marca),
                                    Container(
                                      width: 20.0,
                                      height: 10.0,
                                      color: Color(int.parse(p.color)),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      }).toList(),
                      mainAxisSpacing: 4.0,
                      crossAxisSpacing: 4.0,
                      staggeredTiles: tiles,
                    );
        },
      ),
    );
  }
}

class AgregarPrenda extends StatefulWidget {
  final int idCat;

  AgregarPrenda(this.idCat);

  @override
  _AgregarPrendaState createState() => _AgregarPrendaState();
}

class _AgregarPrendaState extends State<AgregarPrenda> {
  Color ini = Colors.grey;
  io.File i;

  var marca = TextEditingController();

  changeSimpleColor(Color color) {
    ini = color;
  }

  picker(ImageSource s) async {
    io.File img = await ImagePicker.pickImage(source: s);
    if (img != null) {
      //print(img.path);
      setState(() {
        i = img;
      });
    }
  }

  void guardarPrenda() async {
    var db = await DBHelper();
    io.Directory dir = await getApplicationDocumentsDirectory();

    String path = dir.path;
    var prenda = Prenda(
        ini.toString().substring(6, ini.toString().length - 1), marca.text, '');
    await db.guardarPrenda(prenda);
    int id = await db.getID();
    path = path + '/' + id.toString() + i.path.substring(i.path.length - 4);
    print(path);
    await db.actualizaRuta(id, path, widget.idCat);

    await i.copy('$path');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Agregar prenda'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              size: 30.0,
            ),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return SingleChildScrollView(
                      child: AlertDialog(
                        actions: <Widget>[
                          FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('CANCELAR'),
                          ),
                          RaisedButton(
                            onPressed: () {
                              Navigator.pop(context);
                              Navigator.pop(context);
                              guardarPrenda();
                              setState(() {});
                            },
                            child: Text('GUARDAR'),
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            textColor: Colors.white,
                          ),
                        ],
                        title: Text('¿Deseas guardar la siguiente prenda?'),
                        content: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            i == null
                                ? Center(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.grey,
                                        ),
                                      ),
                                      padding: EdgeInsets.all(10.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.broken_image,
                                            size: 100.0,
                                            color: Colors.grey,
                                          ),
                                          Center(
                                            child: Text(
                                              'No se seleccionó una imagen',
                                              style:
                                                  TextStyle(color: Colors.grey),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : Center(
                                    child: Image.file(i),
                                  ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Marca: ',
                                ),
                                Text(marca.text == '' ? ' --- ' : marca.text),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      'Color: ',
                                    ),
                                    DecoratedBox(
                                      decoration: BoxDecoration(color: ini),
                                      child: Container(
                                        height: 20.0,
                                        width: 40.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  });
            },
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10.0,
              ),
              InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text('¿Cámara o galería?'),
                        actions: <Widget>[
                          SimpleDialogOption(
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.camera,
                                  size: 30.0,
                                ),
                                Text(
                                  'Cámara',
                                ),
                              ],
                            ),
                            onPressed: () {
                              picker(ImageSource.camera);
                              Navigator.pop(context);
                            },
                          ),
                          SimpleDialogOption(
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.image,
                                  size: 30.0,
                                ),
                                Text(
                                  'Galería',
                                ),
                              ],
                            ),
                            onPressed: () {
                              picker(ImageSource.gallery);
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                child: Container(
                  height: MediaQuery.of(context).size.height / 1.5,
                  width: MediaQuery.of(context).size.width,
                  child: i == null
                      ? Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.image,
                                size: 100.0,
                                color: Colors.grey,
                              ),
                              Text(
                                'Presiona para cargar una imagen',
                                style: TextStyle(color: Colors.grey),
                              ),
                            ],
                          ),
                        )
                      : Image.file(i),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.grey)),
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 20.0,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Arrastra el círculo para seleccionar el color de la prenda',
                    //style: TextStyle(fontSize: 25.0),
                  ),
                  ColorPicker(
                    pickerColor: ini,
                    onColorChanged: changeSimpleColor,
                    colorPickerWidth: 1000.0,
                    pickerAreaHeightPercent: 0.7,
                    enableAlpha: false,
                    enableLabel: false,
                  ),
                ],
              ),
              Divider(
                color: Colors.grey,
                height: 20.0,
              ),
              TextField(
                controller: marca,
                textCapitalization: TextCapitalization.words,
                autofocus: false,
                decoration: InputDecoration(
                  labelText: 'Marca: ',
                  labelStyle: TextStyle(fontSize: 20.0),
                  contentPadding: EdgeInsets.only(bottom: 3.0),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
