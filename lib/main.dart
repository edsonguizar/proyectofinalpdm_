import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'outfits.dart';
import 'guardarropa.dart';
import 'historial.dart';
import 'estadisticas.dart';

void main() => runApp(MyApp());

double alto(BuildContext context) => MediaQuery.of(context).size.height;
double ancho(BuildContext context) => MediaQuery.of(context).size.width;

/*
PÁGINA PRINCIPAL
*/
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return new AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        sized: false,
        child: MaterialApp(
          title: 'Fit My Clothes',
          theme: ThemeData(
              primaryColor: Color(0xff2c0a2c), fontFamily: 'AvenirNext'),
          home: PaginaInicial(),
          routes: <String, WidgetBuilder>{
            '/outfits': (BuildContext context) => Outfits(),
            '/guardarropa': (BuildContext context) => Guardarropa(),
            '/historial': (BuildContext context) => Historial(),
          },
        ));
  }
}

class PaginaInicial extends StatefulWidget {
  PaginaInicial({Key key}) : super(key: key);

  @override
  _PaginaInicialState createState() => _PaginaInicialState();
}

class _PaginaInicialState extends State<PaginaInicial> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.blue, //or set color with: Color(0xFF0000FF)
    ));
    double h = alto(context);
    double w = ancho(context);
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage('img/background.jpg'),
                fit: BoxFit.cover,
                colorFilter:
                    ColorFilter.mode(Color(0x11747474), BlendMode.darken))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(padding: const EdgeInsets.only(bottom: 10.0)),
            Container(
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage('img/FMC.png'),
                  ),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      blurRadius: 30.0,
                      color: Color.fromARGB(135, 0, 0, 0),
                    )
                  ]),
              width: w / 4,
              height: h / 8,
            ),
            Padding(padding: const EdgeInsets.only(bottom: 30.0)),
            boton('Outfits', context, ruta: '/outfits'),
            Padding(padding: const EdgeInsets.only(bottom: 13.0)),
            boton('Guardarropa', context, ruta: '/guardarropa'),
            Padding(padding: const EdgeInsets.only(bottom: 13.0)),
            boton('Historial', context, ruta: '/historial'),
          ],
        ),
      ),
    );
  }
}

Widget boton(String titulo, BuildContext context,
    {String ruta = '', String nombreVerdadero = ''}) {
  double h = alto(context);
  return FlatButton(
    onPressed: () {
      print(titulo);
      Navigator.of(context).pushNamed(ruta);
    },
    padding: EdgeInsets.all(0.0),
    child: Container(
        height: h / 13,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Color(0x66FFFFFF),
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: new AssetImage('img/' + titulo + '.png'),
                      ),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          blurRadius: 45.0,
                          color: Color.fromARGB(205, 0, 0, 0),
                        )
                      ]),
                  width: h / 30,
                ),
                Text(
                  ' ' + (nombreVerdadero == '' ? titulo : nombreVerdadero),
                  style: TextStyle(
                      fontSize: h / 20,
                      color: Colors.white,
                      shadows: <Shadow>[
                        Shadow(
                          blurRadius: 25.0,
                          color: Color.fromARGB(205, 0, 0, 0),
                        ),
                      ]),
                )
              ],
            ),
          ),
        )),
  );
}
