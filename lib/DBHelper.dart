import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'dart:async';
import 'package:path/path.dart';
import 'guardarropa.dart';
import 'guardarropa/agregarPrenda.dart';
import 'outfits.dart';

class DBHelper {
  static final DBHelper _instance = new DBHelper.internal();
  DBHelper.internal();

  factory DBHelper() => _instance;

  static Database _db;

  static var p;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await setDB();
    return db;
  }

  setDB() async {
    io.Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, 'FMC');

    print(path);

    //deleteDatabase(path);

    var dB = await openDatabase(path,
        version: 1, onCreate: _onCreate, readOnly: false);
    return dB;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE categoria (id_categoria INTEGER PRIMARY KEY, nombre TEXT, tipo INTEGER)');
    await db.execute(
        'CREATE TABLE prenda (id_prenda INTEGER PRIMARY KEY, color TEXT, marca TEXT, ruta TEXT)');
    await db.execute(
        'CREATE TABLE categoria_prenda (id_prenda INTEGER, id_categoria INTEGER)');
    await db.execute(
        'CREATE TABLE outfit (id_outfit INTEGER PRIMARY KEY, nombre TEXT, descripcion TEXT)');
    await db.execute(
        'CREATE TABLE prenda_outfit (id_outfit INTEGER, id_prenda INTEGER)');
    await db
        .execute('CREATE TABLE fecha_outfit (fecha TEXT, id_outfit INTEGER)');
    print('BD creada');
  }

  Future<int> guardarCategoria(Categoria cat) async {
    var dbClient = await db;
    int res = await dbClient.insert('categoria', cat.toMap());
    return res;
  }

  Future<List<Categoria>> getCategorias() async {
    var dbClient = await db;

    List<Map> rows =
        await dbClient.rawQuery('SELECT * FROM categoria ORDER BY nombre ASC');
    List<Categoria> cats = new List();
    for (int i = 0; i < rows.length; i++) {
      var cat = new Categoria(rows[i]['nombre'], rows[i]['tipo']);
      cat.setID(rows[i]['id_categoria']);
      cats.add(cat);
    }
    return cats;
  }

  Future<int> eliminarCategoria(int cat) async {
    var dbClient = await db;
    int res = await dbClient
        .rawDelete('DELETE FROM categoria WHERE id_categoria = ?', [cat]);
    return res;
  }

  Future<List<Prenda>> getPrendas(int idCat) async {
    var dbClient = await db;
    List<Map> rows = await dbClient.rawQuery(
        'SELECT * FROM prenda INNER JOIN categoria_prenda ON categoria_prenda.id_prenda = prenda.id_prenda WHERE categoria_prenda.id_categoria = ? ORDER BY id_prenda DESC',
        [idCat]);
    List<Prenda> prendas = new List();
    for (int i = 0; i < rows.length; i++) {
      var prenda = Prenda(rows[i]['color'], rows[i]['marca'], rows[i]['ruta']);
      prenda.setID(rows[i]['id_prenda']);
      prendas.add(prenda);
    }
    return prendas;
  }

  Future<int> guardarPrenda(Prenda prenda) async {
    var dbClient = await db;
    int res = await dbClient.insert('prenda', prenda.toMap());
    return res;
  }

  Future<int> getID() async {
    var dbClient = await db;
    List<Map> row =
        await dbClient.rawQuery('SELECT last_insert_rowid() as last_id');
    return row[0]['last_id'];
  }

  Future<int> actualizaRuta(int id, String path, int cat) async {
    var dbClient = await db;
    int res = await dbClient.rawUpdate(
        'UPDATE prenda SET ruta = ? WHERE id_prenda = ?', [path, id]);
    await dbClient
        .rawInsert('INSERT INTO categoria_prenda VALUES ( ? , ? )', [id, cat]);
    return res;
  }

  Future<List<Outfit>> getOutfits() async {
    var dbClient = await db;
    List<Map> rows =
        await dbClient.rawQuery('SELECT * FROM outfit ORDER BY id_outfit DESC');
    List<Outfit> outfits = new List();
    for (int i = 0; i < rows.length; i++) {
      var outfit = Outfit(rows[i]['nombre'], rows[i]['descripcion']);
      outfit.setID(rows[i]['id_outfit']);
      outfits.add(outfit);
    }
    return outfits;
  }

  Future<List<Prenda>> getPrendasOutfit(int idOutfit) async {
    var dbClient = await db;
    List<Map> rows = await dbClient.rawQuery(
        'SELECT * FROM prenda_outfit WHERE id_outfit = ?', [idOutfit]);
    List<Prenda> prendas = List();
    for (int i = 0; i < rows.length; i++) {
      List<Map> row = await dbClient.rawQuery(
          'SELECT * FROM prenda WHERE id_prenda = ?', [rows[i]['id_prenda']]);
      var prenda = Prenda(row[0]['color'], row[0]['marca'], row[0]['ruta']);
      prenda.setID(rows[0]['id_prenda']);
      prendas.add(prenda);
    }
    return prendas;
  }

  Future<List<Map>> getPrendasOutfits() async {
    var dbClient = await db;
    List<Map> rows = await dbClient.rawQuery('SELECT * FROM prenda_outfit');
    return rows;
  }

  Future<int> guardarOutfit(Outfit o) async {
    var dbClient = await db;
    await dbClient.insert('outfit', o.toMap());
    List<Map> row =
        await dbClient.rawQuery('SELECT last_insert_rowid() as last_id');
    return row[0]['last_id'];
  }

  Future<int> guardarPrendaOutfit(int idO, int idP) async {
    var dbClient = await db;
    int res = await dbClient
        .rawInsert('INSERT INTO prenda_outfit VALUES ( ? , ? )', [idO, idP]);
    //print('Guardé: ' + idO.toString() + ' -- ' + idP.toString());
    return res;
  }

  Future<int> guardarFechaOutfit(String fecha, int idO) async {
    var dbClient = await db;
    int res = await dbClient
        .rawInsert('INSERT INTO fecha_outfit VALUES ( ? , ? )', [fecha, idO]);
    return res;
  }

  Future<Map> getOutfit(String fecha) async {
    var dbClient = await db;
    List<Map> row = await dbClient.rawQuery(
        'SELECT * FROM fecha_outfit WHERE fecha = ? ORDER BY id_outfit ASC',
        [fecha]);
    int idO = row[0]['id_outfit'];
    List<Map> out = await dbClient
        .rawQuery('SELECT * FROM outfit WHERE id_outfit = ?', [idO]);

    return out[0];
  }

  Future<int> eliminarHistorial(int idOut, String fecha) async {
    var dbClient = await db;
    int res = await dbClient.rawDelete(
        'DELETE FROM fecha_outfit WHERE id_outfit = ? AND fecha = ?',
        [idOut, fecha]);
    return res;
  }
}
