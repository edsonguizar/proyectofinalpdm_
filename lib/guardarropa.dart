import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'DBHelper.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'guardarropa/agregarPrenda.dart';

double alto(BuildContext context) => MediaQuery.of(context).size.height;
double ancho(BuildContext context) => MediaQuery.of(context).size.width;

class Categoria {
  int id;
  String _nombre;
  int _tipo;

  Categoria(this._nombre, this._tipo);

  Categoria.map(dynamic obj) {
    this._nombre = obj['nombre'];
    this._tipo = obj['tipo'];
  }

  String get nombre => _nombre;
  int get tipo => _tipo;

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['nombre'] = _nombre;
    map['tipo'] = _tipo;

    return map;
  }

  void setID(int id) {
    this.id = id;
  }
}

class Guardarropa extends StatefulWidget {
  Guardarropa({Key key}) : super(key: key);

  @override
  _GuardarropaState createState() => _GuardarropaState();
}

class AgregarCategoria extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  AgregarCategoria(this.scaffoldKey);
  @override
  _AgregarCategoriaState createState() => new _AgregarCategoriaState();
}

class _AgregarCategoriaState extends State<AgregarCategoria> {
  final cNombre = TextEditingController();
  String nombreCategoria;
  int tipoCategoria = 0;
  int groupRadio = 0;
  final formKey = new GlobalKey<FormState>();

  void handleRadioValueChanged(int value) {
    setState(() {
      groupRadio = value;
    });
  }

  void _showSnackBar(String text) {
    widget.scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
      duration: Duration(seconds: 2),
    ));
  }

  Future guardarCategoria() async {
    var db = await DBHelper();
    var cat = Categoria(cNombre.text, groupRadio);
    await db.guardarCategoria(cat);
    print('Categoría guardada');
    Navigator.pop(context);
    _showSnackBar("Categoría guardada: " + cNombre.text);
    setState(() {});
  }

  void _submit() {
    if (this.formKey.currentState.validate()) {
      formKey.currentState.save();
      guardarCategoria();
    }
  }

  @override
  Widget build(BuildContext context) {
    double w = ancho(context);
    double h = alto(context);
    return new SingleChildScrollView(
      padding: EdgeInsets.all(0.0),
      child: new AlertDialog(
        titlePadding: EdgeInsets.only(top: h / 30),
        title: Center(
          child: const Text(
            'Nueva categoría',
            style: TextStyle(fontSize: 25.0),
          ),
        ),
        content: new Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Form(
              key: formKey,
              child: new Column(
                children: <Widget>[
                  new TextFormField(
                    keyboardType: TextInputType.text,
                    controller: cNombre,
                    textCapitalization: TextCapitalization.sentences,
                    autofocus: true,
                    style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.black87,
                    ),
                    scrollPadding: const EdgeInsets.all(0.0),
                    decoration: new InputDecoration(
                      hintText: 'Nombre',
                    ),
                    validator: (val) =>
                        val.length == 0 ? "Ingresa el nombre" : null,
                    onSaved: (val) => this.nombreCategoria = val,
                  ),
                  new Padding(
                    padding: EdgeInsets.only(top: h / 25),
                  ),
                  new Text(
                    'Tipo de categoría:',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  new RadioListTile<int>(
                    value: 0,
                    groupValue: groupRadio,
                    onChanged: handleRadioValueChanged,
                    title: Text('Prendas'),
                    subtitle: Text('Blusas, tacones, collares, etc.'),
                  ),
                  new RadioListTile<int>(
                    value: 1,
                    groupValue: groupRadio,
                    onChanged: handleRadioValueChanged,
                    title: Text('Ocasión'),
                    subtitle: Text('Invierno, fiesta, antro, etc.'),
                  ),
                ],
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
            child: const Text('CANCELAR'),
            onPressed: () {
              Navigator.pop(context);
            },
            padding: EdgeInsets.only(right: w / 50),
          ),
          new RaisedButton(
            textColor: Colors.white,
            child: const Text(
              'GUARDAR',
              style: TextStyle(letterSpacing: 0.0),
            ),
            onPressed: _submit,
            padding: EdgeInsets.all(5.0),
          )
        ],
        contentPadding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
      ),
    );
  }
}

class _GuardarropaState extends State<Guardarropa> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var db = new DBHelper();

  List<Categoria> cats;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(
          'Guardarropa',
          style: TextStyle(fontSize: 30.0),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              size: 30.0,
            ),
            onPressed: () => showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AgregarCategoria(scaffoldKey);
                }),
            tooltip: 'Agregar categoría',
          ),
        ],
      ),
      body: FutureBuilder(
        future: db.getCategorias(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          cats = snapshot.data;

          return !snapshot.hasData
              ? Center(
                  child: Text('No hay categorías'),
                )
              : cats.isEmpty
                  ? Center(
                      child: Text('No hay categorías'),
                    )
                  : ListView.builder(
                      itemCount: cats.length,
                      itemBuilder: (BuildContext context, int i) {
                        return Slidable(
                          delegate: SlidableDrawerDelegate(),
                          closeOnScroll: true,
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  title: Text(cats[i].nombre.toString()),
                                  onTap: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          new PrendasCat(cats[i].id,
                                              cats[i].nombre.toString()),
                                    ))
                                        .then((value) {
                                      setState(() {});
                                    });
                                  },
                                  trailing: Icon(Icons.keyboard_arrow_right),
                                ),
                                Divider(
                                  color: Colors.grey,
                                  height: 0.0,
                                )
                              ],
                            ),
                          ),
                          secondaryActions: <Widget>[
                            IconSlideAction(
                              foregroundColor: Colors.white,
                              caption: 'Editar',
                              color: Colors.grey,
                              icon: Icons.edit,
                              onTap: () {},
                            ),
                            IconSlideAction(
                              caption: 'Eliminar',
                              color: Colors.red,
                              icon: Icons.delete_forever,
                              onTap: () {
                                eliminarCategoria(cats[i].id);
                              },
                            ),
                          ],
                        );
                      },
                    );
        },
      ),
    );
  }

  Future eliminarCategoria(int cat) async {
    var db = await DBHelper();
    await db.eliminarCategoria(cat);
    print('Categoría eliminada');
    setState(() {});
    //Navigator.pop(context);
    //_showSnackBar("Categoría guardada: " + cNombre.text);
  }
}
